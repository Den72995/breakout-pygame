import pygame
from random import randint

BLACK = (0, 0, 0)


class Ball(pygame.sprite.Sprite):
    def __init__(self, color, width, height):
        super().__init__()

        self.image = pygame.Surface([width, height])
        self.image.fill(BLACK)
        self.image.set_colorkey(BLACK)

        pygame.draw.rect(self.image, color, [0, 0, width, height])
        self.velocity = [randint(4, 6), randint(-6, 6)]
        while self.velocity[0] == 0 or self.velocity[1] == 0:
            self.velocity = [randint(4, 6), randint(-6, 6)]
        self.rect = self.image.get_rect()

    def update(self):
        self.rect.x += self.velocity[0]
        self.rect.y += self.velocity[1]

    def bounce(self):
        self.velocity[0] = -self.velocity[0]
        self.velocity[1] = randint(-6, 6)


class Paddle(pygame.sprite.Sprite):
    def __init__(self, color, width, height):
        super().__init__()

        self.image = pygame.Surface([width, height])
        self.image.fill(BLACK)
        self.image.set_colorkey(BLACK)

        pygame.draw.rect(self.image, color, [0, 0, width, height])

        self.rect = self.image.get_rect()

    def moveLeft(self, pixels):
        self.rect.x -= pixels

        if self.rect.x < 0:
            self.rect.x = 0

    def moveRight(self, pixels):
        self.rect.x += pixels

        if self.rect.x > 700:
            self.rect.x = 700


class Brick(pygame.sprite.Sprite):

    def __init__(self, color, width, height):
        super().__init__()
        self.life = 0

        self.cl = color
        self.image = pygame.Surface([width, height])
        self.image.fill(BLACK)
        self.image.set_colorkey(AfterColors[self.life])



        pygame.draw.rect(self.image, self.cl, [0, 0, width, height])

        self.rect = self.image.get_rect()

    def Update(self):
        self.life += 1
        if self.life == len(AfterColors):
            self.kill()


        self.cl = AfterColors[self.life]




# region Initialization
pygame.init()
size = (800, 600)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Breakout")

clock = pygame.time.Clock()
# endregion

# region colors
WHITE = (255, 255, 255)
DARKBLUE = (36, 90, 190)
PurpleMountainMajesty = (152, 120, 196)
LIGHTBLUE = (0, 176, 240)
RED = (255, 0, 0)
ORANGE = (255, 100, 0)
YELLOW = (255, 255, 0)

colors = [(120, 139, 196), (0, 0, 0)]
AfterColors = [(152, 123, 193), (154, 134, 182), (158, 155, 161)]
rocketColor = (139, 196, 120)
# endregion

score = 0
lives = 3

XLocation = 0
v = 0
pressed_left = False
pressed_right = False

paddle = Paddle(rocketColor, 100, 20)
paddle.rect.x = 400
paddle.rect.y = 480

ball = Ball(WHITE, 10, 10)
ball.rect.x = 345
ball.rect.y = 195


all_sprites_list = pygame.sprite.Group()
all_sprites_list.add(ball)
all_sprites_list.add(paddle)

all_bricks = pygame.sprite.Group()
for i in range(7):
    brick = Brick(AfterColors[0] ,80,30)
    brick.rect.x = 60 + i* 100
    brick.rect.y = 60
    all_sprites_list.add(brick)
    all_bricks.add(brick)
for i in range(7):
    brick = Brick(AfterColors[0],80,30)
    brick.rect.x = 60 + i* 100
    brick.rect.y = 100
    all_sprites_list.add(brick)
    all_bricks.add(brick)
for i in range(7):
    brick = Brick(AfterColors[0],80,30)
    brick.rect.x = 60 + i* 100
    brick.rect.y = 140
    all_sprites_list.add(brick)
    all_bricks.add(brick)

speed = 10

while True:
    # region text
    screen.fill(colors[0])

    font = pygame.font.Font(None, 34)
    text = font.render("Score: " + str(score), 1, YELLOW)
    screen.blit(text, (20, 550))
    text = font.render("Lives: " + str(lives), 1, YELLOW)
    screen.blit(text, (650, 550))

    pygame.draw.line(screen, WHITE, [0, 520], [800, 520], 2)
    # endregion text

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                pressed_left = False
            elif event.key == pygame.K_RIGHT:
                pressed_right = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                pressed_left = True
            elif event.key == pygame.K_RIGHT:
                pressed_right = True

    # region Paddle
    if pressed_left:
        paddle.moveLeft(speed)
    if pressed_right:
        paddle.moveRight(speed)

    pygame.draw.rect(screen, rocketColor, (paddle.rect.x, paddle.rect.y, paddle.rect.width, paddle.rect.height))
    # endregion Paddle

    all_sprites_list.update()

    # region Ball
    if pygame.sprite.collide_mask(ball, paddle):
        ball.rect.x -= ball.velocity[0]
        ball.rect.y -= ball.velocity[1]
        ball.bounce()

    if ball.rect.x >= 790:
        ball.velocity[0] = -ball.velocity[0]
    if ball.rect.x <= 0:
        ball.velocity[0] = -ball.velocity[0]
    if ball.rect.y > 510:
        lives -= 1
        if lives == 0:
            font = pygame.font.Font(None, 74)
            text = font.render("GAME OVER", 1, WHITE)
            screen.blit(text, (250, 300))
            pygame.display.flip()
            pygame.time.wait(3000)
            pygame.quit()
        else:
            font = pygame.font.Font(None, 74)
            text = font.render("-1 LIFE", 1, WHITE)
            screen.blit(text, (250, 300))
            pygame.display.flip()
            pygame.time.wait(1500)
            ball.rect.x = 345
            ball.rect.y = 195
            ball.velocity = [randint(4, 8), randint(-8, 8)]

    if ball.rect.y < 0:
        ball.velocity[1] = -ball.velocity[1]
    # endregion


    brick_collision_list = pygame.sprite.spritecollide(ball, all_bricks, False)
    for brick in brick_collision_list:
        ball.bounce()
        score += 10
        brick.update()
        if len(all_bricks) == 0:
            # Display Level Complete Message for 3 seconds
            font = pygame.font.Font(None, 74)
            text = font.render("LEVEL COMPLETE", 1, WHITE)
            screen.blit(text, (200, 300))
            pygame.display.flip()
            pygame.time.wait(3000)

    all_sprites_list.draw(screen)



    pygame.display.flip()

    clock.tick(60)
